<!DOCTYPE html>
<html lang="en">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String username= (String) session.getAttribute("username");
%>

<head>
<meta charset="utf-8">
<meta name="description" content="12">
<meta name="author" content="12">
<meta name="keyword" content="12">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>首页</title>

<link rel="stylesheet" type="text/css" href="asset/css/bootstrap.min.css">

<link rel="stylesheet" type="text/css" href="asset/css/plugins/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="asset/css/plugins/simple-line-icons.css"/>
<link rel="stylesheet" type="text/css" href="asset/css/plugins/animate.min.css"/>
<link rel="stylesheet" type="text/css" href="asset/css/plugins/fullcalendar.min.css"/>
<link href="asset/css/style.css" rel="stylesheet">
</head>

<body id="mimin" class="dashboard">
<!-- start: Header -->
<nav class="navbar navbar-default header navbar-fixed-top">
  <div class="col-md-12 nav-wrapper">
    <div class="navbar-header" style="width:100%;">
      <div class="opener-left-menu is-open"> 
        <span class="top"></span> 
        <span class="middle"></span> 
        <span class="bottom"></span> 
      </div>
      <a href="news_back.jsp" class="navbar-brand"> <b>首页</b> </a>
      <ul class="nav navbar-nav navbar-right user-nav">
        <li style="display: none;" id="username_session"><%=username%></li>
        <li class="user-name"><span><%=username%></span></li>
        <li class="dropdown avatar-dropdown">
            <img src="asset/img/avatar.jpg" class="img-circle avatar" alt="user name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"/>
          <ul class="dropdown-menu user-dropdown">
              <li><a href="#"><p value="<%=username%>" id="backli" onclick="back()"><span class="fa fa-power-off"></span> 退出登录</p></a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>
<!-- end: Header -->
<div class="container-fluid mimin-wrapper"> 
  <!-- start:Left Menu -->
  <div id="left-menu">
    <div class="sub-left-menu scroll">
      <ul class="nav nav-list">
        <li>
          <div class="left-bg"></div>
        </li>
        <li class="ripple">
          <a class="tree-toggle nav-header"><span class="fa fa-check-square-o"></span> 新闻管理 
            <span class="fa-angle-right fa right-arrow text-right"></span> 
          </a> 
          <ul class="nav nav-list tree">
              <li><a href="newslist.jsp">新闻列表</a></li>
              <li><a href="newsupload.jsp">发表新闻</a></li>
          </ul>
        </li>
        <li class="active ripple">
          <a class="tree-toggle nav-header"><span class="fa-home fa"></span> 用户管理
            <span class="fa-angle-right fa right-arrow text-right"></span>
          </a>
        </li>
      </ul>
    </div>
  </div>
  <!-- end: Left Menu -->
  <!-- start: content -->
  <div id="content">
    <div class="panel">
      <div class="panel-body">
        <div class="col-md-6 col-sm-12">
          <h3 class="animated fadeInLeft">欢迎您：<%=username%></h3>
          <p class="animated fadeInDown"><span class="fa  fa-map-marker"></span> 后台</p>
        </div>
      </div>
    </div>
    <%--新闻列表--%>
    <div class="col-md-12 padding-0 form-element">
      <div class="col-md-12">
        <div class="panel form-element-padding">

          <div class="panel">
            <div class="panel-heading">
              <h3>列表名称</h3>
            </div>
            <div class="panel-body">
              <div class="responsive-table">
                <div id="datatables-example_wrapper"
                     class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                  <div class="row">
                    <div class="col-md-12">
                      <table  class="table table-hover"  id="mytable_admin_news">
                        <thead><tr role="row">
                          <th class="sorting_asc" >id</th>
                          <th class="sorting_asc" >标题</th>
                          <th class="sorting" >作者</th>
                          <th class="sorting" >发表时间</th>
                          <th class="sorting" >阅览数</th>
                          <th class="sorting">点击查看</th>
                          <th class="sorting">操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                    <!-- 显示分页  -->
                    <div class="row">
                      <div class="col-md-6"  id="pageinfo_area_news">
                      </div>
                      <div class="col-md-6"  id="pageinfog_news">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end: content -->
</div>


<!-- Modal -->
<div class="modal fade" id="login_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">提示</h4>
      </div>
      <div class="modal-body">
       <h3 style="text-align:center">请登录</h3>
      </div>
      <div class="modal-footer">
        <a href="/news/adminlogin" class="button">确定</a>
      </div>
    </div>
  </div>
</div>
<!-- start: Javascript --> 
<script src="asset/js/jquery.min.js"></script> 
<script src="asset/js/jquery.ui.min.js"></script> 
<script src="asset/js/bootstrap.min.js"></script> 
<!-- plugins --> 
<script src="asset/js/plugins/jquery.nicescroll.js"></script> 
<!-- custom --> 
<script src="asset/js/main.js"></script>
<script src="asset/js/moment.js"></script>
<script src="js/Confim.js"></script>
<script src="js/admin.js"></script>
<!-- end: Javascript -->
</body>
</html>