<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String username = (String) session.getAttribute("username");
%>
<head>
    <meta charset="utf-8">
    <meta name="description" content="xxxxx">
    <meta name="author" content="xxxxx">
    <meta name="keyword" content="xxxxx">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>新闻列表</title>
    <!-- start: Css -->
    <link rel="stylesheet" type="text/css" href="asset/css/bootstrap.min.css">
    <!-- plugins -->
    <link rel="stylesheet" type="text/css" href="asset/css/plugins/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="asset/css/plugins/simple-line-icons.css"/>
    <link rel="stylesheet" type="text/css" href="asset/css/plugins/animate.min.css"/>
    <link rel="stylesheet" type="text/css" href="asset/css/plugins/fullcalendar.min.css"/>
    <link href="asset/css/style.css" rel="stylesheet">
    <!-- end: Css -->
    <%--<script src="bootstraptable/bootstrap-table.js"></script>--%>
    <%--<link href="bootstraptable/bootstrap-table.css" rel="stylesheet" />--%>
    <%--<script src="bootstraptable/locale/bootstrap-table-zh-CN.js"></script>--%>
</head>

<body id="mimin" class="dashboard">
<!-- start: Header -->
<nav class="navbar navbar-default header navbar-fixed-top">
    <div class="col-md-12 nav-wrapper">
        <div class="navbar-header" style="width:100%;">
            <div class="opener-left-menu is-open"><span class="top"></span> <span class="middle"></span> <span
                    class="bottom"></span></div>
            <a href="/news/home" class="navbar-brand"> <b>返回首页</b> </a>
            <ul class="nav navbar-nav navbar-right user-nav">
                <li style="display: none;" id="username_session"><%=username%>
                </li>
                <li class="user-name" id="username_show"><span><%=username%></span></li>
                <li class="dropdown avatar-dropdown"><img src="asset/img/avatar.jpg" class="img-circle avatar"
                                                          alt="user name" data-toggle="dropdown" aria-haspopup="true"
                                                          aria-expanded="true"/>
                    <ul class="dropdown-menu user-dropdown" id="logout_home">
                        <li><a href="#"><p value="<%=username%>" id="backli" onclick="back()"><span
                                class="fa fa-power-off"></span> 退出登录</p></a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- end: Header -->
<div class="container-fluid mimin-wrapper">
    <!-- start:Left Menu -->
    <div id="left-menu">
        <div class="sub-left-menu scroll">
            <ul class="nav nav-list" id="news_categorylist">
                <li>
                    <div class="left-bg"></div>
                </li>
                <li class="ripple">
                    <a class="tree-toggle nav-header" id="left-menu-1"><a href="news_back.jsp"><span
                            class="fa-home fa"></span> 首页
                        <span class="fa-angle-right fa right-arrow text-right"></span></a>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <!-- end: Left Menu -->

    <!-- start: content -->
    <div id="content">
        <div class="panel">
            <div class="panel-body">
                <div class="col-md-12">
                    <h3 class="animated fadeInLeft">新闻列表</h3>
                </div>
            </div>
        </div>
        <div class="col-md-12 padding-0 form-element">
            <div class="col-md-12">
                <div class="panel form-element-padding">

                    <div class="panel">
                        <div class="panel-heading">
                            <h3>列表名称</h3>
                        </div>
                        <div class="panel-body">
                            <div class="responsive-table">
                                <div id="datatables-example_wrapper"
                                     class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table  class="table table-hover"  id="mytable_user">
                                                <thead><tr role="row">
                                                    <th class="sorting_asc" style="width: 60px;">标题</th>
                                                    <th class="sorting" style="width: 60px;">作者</th>
                                                    <th class="sorting" style="width: 60px;">发表时间</th>
                                                    <th class="sorting" style="width: 60px;">阅览数</th>
                                                    <th class="sorting" style="width: 61px;">点击查看</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- 显示分页  -->
                                        <div class="row">
                                            <div class="col-md-6"  id="pageinfo_area">
                                            </div>
                                            <div class="col-md-6"  id="pageinfog">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end: content -->
</div>
<!-- start: Javascript -->
<script src="asset/js/jquery.min.js"></script>
<script src="asset/js/jquery.ui.min.js"></script>
<script src="asset/js/bootstrap.min.js"></script>
<!-- plugins -->
<script src="asset/js/plugins/jquery.nicescroll.js"></script>
<!-- custom -->
<script src="asset/js/main.js"></script>
<script src="asset/js/moment.js"></script>
<script src="js/Confim.js"></script>
<script src="js/home.js"></script>
<!-- end: Javascript -->
<script>
    $("#left-menu-2").click();
</script>
</body>
</html>