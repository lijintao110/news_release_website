Date.prototype.Format = function (fmt) { //author: meizz
    var o = {
        "M+": this.getMonth() + 1, //月份
        "d+": this.getDate(), //日
        "h+": this.getHours(), //小时
        "m+": this.getMinutes(), //分
        "s+": this.getSeconds(), //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}
/**
 * 用户为登录
 */
if ($("#username_session").html() == "null") {
    $("#login_Modal").modal({
        backdrop: "static"
    });
}


var pagetotal_admin_news;
$(function () {
    to_page_admin_news(1);
});

function to_page_admin_news(pn) {
    $.ajax({

        url: "/news/newslist1",
        data: "pn=" + pn,
        type: "GET",
        datatype: "json",
        success: function (result) {
            //解析并显示员工数据
            build_emp_info_admin_news(result);
            //解析并显示分页信息
            build_page_info_admin_news(result);
            build_pagenav_info_admin_news(result);
        }
    });
}

//解析并显示员工数据

function build_emp_info_admin_news(result) {
    $("#mytable_admin_news tbody").empty();
    var emp = result.extend.pageinfo.list;
    $.each(emp, function (index, items) {
        var dempName = $("<td> </td>").append(items.newsid);
        var dgender = $("<td> </td>").append(items.newsitem);
        var demail = $("<td> </td>").append(items.newsauthor);
        var dempid = $("<td> </td>").append(items.newsdate);
        var ddeptName = $("<td> </td>").append(items.newsreadnum);
        var ddeptlook = $("<td> </td>").append("<a href=\"/news/newsitem?newsid=" + items.newsid + "\">查看</a>");
        var debutton = $("<button></button>").addClass("btn btn-danger btn-xs  dele_dutton")
            .append($(" <span></span>").addClass("glyphicon glyphicon-remove-circle")).append("删除");
        $("<tr></tr>")
            .append(dempName)
            .append(dgender)
            .append(demail)
            .append(dempid)
            .append(ddeptName)
            .append(ddeptlook)
            .append(debutton)
            .appendTo("#mytable_admin_news tbody");
    });
}
function build_page_info_admin_news(result) {
    $("#pageinfo_area_news").empty();
    $("#pageinfo_area_news").append("当前" + result.extend.pageinfo.pageNum + ",总" + result.extend.pageinfo.pages + "页,总" + result.extend.pageinfo.total + "条记录");
    pagetotal_admin_news = result.extend.pageinfo.pages;
}
function build_pagenav_info_admin_news(result) {
    $("#pageinfog_news").empty();
    var ur = $("<ul></ul>").addClass("pagination");
    var firstpage = $("<li></li>").append($("<a></a>").append("首页").attr("href", "#"));
    var prepage = $("<li></li>").append($("<a></a>").append("&raquo;"));
    if (result.extend.pageinfo.hasPreviousPage == false) {
        firstpage.addClass("disabled");
        prepage.addClass("disabled");
    }
    else {
        firstpage.click(function () {
            to_page(1);
        });
        prepage.click(function () {
            to_page(result.extend.pageinfo.pageNum + 1);
        });
    }
    var nextpage = $("<li></li>").append($("<a></a>").append("&laquo;"));
    var lastpage = $("<li></li>").append($("<a></a>").append("末页").attr("href", "#"));
    if (result.extend.pageinfo.hasNextPage == false) {
        lastpage.addClass("disabled");
        nextpage.addClass("disabled");
    }
    else {
        nextpage.click(function () {
            to_page(result.extend.pageinfo.pageNum - 1);
        });
        lastpage.click(function () {
            to_page(result.extend.pageinfo.pages);
        });
    }
    ur.append(firstpage).append(nextpage);
    var emp = result.extend.pageinfo.navigatepageNums;
    $.each(emp, function (index, items) {
        var f = $("<li></li>").append($("<a></a>").append(items));
        if (result.extend.pageinfo.pageNum == items) {
            f.addClass("active");
        }
        f.click(function () {
            to_page(items);
        });
        ur.append(f);
    });
    ur.append(prepage).append(lastpage);
    var nav = $("<nav></nav>").append(ur);
    nav.appendTo("#pageinfog_news");
}