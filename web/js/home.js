Date.prototype.Format = function (fmt) { //author: meizz
    var o = {
        "M+": this.getMonth() + 1, //月份
        "d+": this.getDate(), //日
        "h+": this.getHours(), //小时
        "m+": this.getMinutes(), //分
        "s+": this.getSeconds(), //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}
/**
 * 用户为登录
 */
if ($("#username_session").html() == "null") {
    $("#username_show").html("<a href=\"/news/login\">请登录</a>");
    $("#welcome_title").hide();
    $("#user_panel div div  h4").html("<a href=\"/news/login\">请登录</a>");
    $("#user_panel .panel-body div div h3").html("0");
    $("#logout_home").hide();
}
/**
 * 处理新闻json
 */
$.ajax({
    type: "POST",
    url: "/news/newslist",
    success: function (msg) {
        $.each(msg, function (index, obj) {
            var title = obj['newsitem'];
            var date = new Date(obj['newsdate']);
            var newsid = obj['newsid'];
            var newsdate = date.Format("yyyy年MM月dd hh:mm:ss");
            $("#news_list").append("<li class=\"text-left\"><span class=\"right\">" + newsdate + "</span><a href=\"/news/newsitem?newsid=" + newsid + "\"><span class=\"fa fa-angle-right\">" + title + "</span> </a></li>");
            $("#hot_newslist").append("<li class=\"text-left\"><span class=\"right\">" + newsdate + "</span><a href=\"/news/newsitem?newsid=" + newsid + "\"><span class=\"fa fa-angle-right\">" + title + "</span> </a></li>");
            console.log(title + newsdate);
        });
    }
});
/**
 * 处理新闻类别
 */

$.ajax({
    type: "POST",
    url: "/news/news_category",
    success: function (msg) {
        $.each(msg, function (index, obj) {
            var title = obj['news_category_name'];
            $("#news_categorylist").append("<li class=\"active ripple\"><a class=\"tree-toggle nav-header\"><span class=\"fa-home fa\"></span>" + title + "<span class=\"fa-angle-right fa right-arrow text-right\"></span></a></li>");
        });
    }
});
// $.each(newslist, function (i, val) { //两个参数，第一个参数表示遍历的数组的下标，第二个参数表示下标对应的值
//     console.log(i + "   " + val);
// });
/**
 * 新闻表格
 */
$.ajax({
    type: "POST",
    url: "/news/newslist",
    success: function (msg) {
        $.each(msg, function (index, obj) {
            console.log(index + "   " + obj['newsitem']);
            var date = new Date(obj['newsdate']);
            var newsdate = date.Format("yyyy年MM月dd hh:mm:ss");
            $("#newslist_table").append("<tr><td class=\"sorting_1\">" + obj['newsitem'] + "</td><td>" + obj['newsauthor'] + "</td><td>" + newsdate + "</td><td>" + obj['newsreadnum'] + "</td><td><a href=\"/news/newsitem?newsid=" + obj['newsid'] + "\">查看</a></td></tr>");
        });
    }
});
$("#gotoInfo").click(function () {
    $(window).attr('location', '/news/userinfo');
});

var pagetotal_user;
$(function () {
    to_page_user(1);
});

function to_page_user(pn) {
    $.ajax({
        url: "/news/newslist1",
        data: "pn=" + pn,
        type: "GET",
        datatype: "json",
        success: function (result) {
            //解析并显示员工数据
            build_emp_info_user(result);
            //解析并显示分页信息
            build_page_info_user(result);
            build_pagenav_info_user(result);
        }
    });
}

//解析并显示员工数据

function build_emp_info_user(result) {
    $("#mytable_user tbody").empty();
    var emp = result.extend.pageinfo.list;

    $.each(emp, function (index, items) {
        console.log(items.newsitem);
        var dgender = $("<td> </td>").append(items.newsitem);
        var demail = $("<td> </td>").append(items.newsauthor);
        var dempid = $("<td> </td>").append(items.newsdate);
        var ddeptName = $("<td> </td>").append(items.newsreadnum);
        var ddeptlook = $("<td> </td>").append("<a href=\"/news/newsitem?newsid=" + items.newsid + "\">查看</a>");
        $("<tr></tr>")
            .append(dgender)
            .append(demail)
            .append(dempid)
            .append(ddeptName)
            .append(ddeptlook)
            .appendTo("#mytable_user tbody");
    });
}

function build_page_info_user(result) {

    $("#pageinfo_area").empty();
    $("#pageinfo_area").append("当前" + result.extend.pageinfo.pageNum + ",总" + result.extend.pageinfo.pages + "页,总" + result.extend.pageinfo.total + "条记录");
    pagetotal_user = result.extend.pageinfo.pages;
}

function build_pagenav_info_user(result) {
    $("#pageinfog").empty();
    var ur = $("<ul></ul>").addClass("pagination");
    var firstpage = $("<li></li>").append($("<a></a>").append("首页").attr("href", "#"));
    var prepage = $("<li></li>").append($("<a></a>").append("&raquo;"));
    if (result.extend.pageinfo.hasPreviousPage == false) {
        firstpage.addClass("disabled");
        prepage.addClass("disabled");
    }
    else {
        firstpage.click(function () {
            to_page(1);
        });
        prepage.click(function () {
            to_page(result.extend.pageinfo.pageNum + 1);
        });
    }
    var nextpage = $("<li></li>").append($("<a></a>").append("&laquo;"));
    var lastpage = $("<li></li>").append($("<a></a>").append("末页").attr("href", "#"));
    if (result.extend.pageinfo.hasNextPage == false) {
        lastpage.addClass("disabled");
        nextpage.addClass("disabled");
    }
    else {
        nextpage.click(function () {
            to_page(result.extend.pageinfo.pageNum - 1);
        });
        lastpage.click(function () {
            to_page(result.extend.pageinfo.pages);
        });
    }
    ur.append(firstpage).append(nextpage);
    var emp = result.extend.pageinfo.navigatepageNums;
    $.each(emp, function (index, items) {
        var f = $("<li></li>").append($("<a></a>").append(items));
        if (result.extend.pageinfo.pageNum == items) {
            f.addClass("active");
        }
        f.click(function () {
            to_page(items);
        });
        ur.append(f);
    });
    ur.append(prepage).append(lastpage);
    var nav = $("<nav></nav>").append(ur);
    nav.appendTo("#pageinfog");
}


var pagetotal;
$(function () {
    to_page(1);
});

function to_page(pn) {
    $.ajax({

        url: "/news/newslist1",
        data: "pn=" + pn,
        type: "GET",
        datatype: "json",
        success: function (result) {
            //解析并显示员工数据
            build_emp_info(result);
            //解析并显示分页信息
            build_page_info(result);
            build_pagenav_info(result);
        }
    });
}

//解析并显示员工数据

function build_emp_info(result) {
    $("#mytable tbody").empty();
    var emp = result.extend.pageinfo.list;
    $.each(emp, function (index, items) {
        var dempName = $("<td> </td>").append(items.newsid);
        var dgender = $("<td> </td>").append(items.newsitem);
        var demail = $("<td> </td>").append(items.newsauthor);
        var dempid = $("<td> </td>").append(items.newsdate);
        var ddeptName = $("<td> </td>").append(items.newsreadnum);
        var dbutton = $("<button></button>").addClass("btn btn-primary btn-xs   edi_dutton")
            .append($(" <span></span>").addClass("glyphicon glyphicon-pencil")).append(" 编辑");
        var debutton = $("<button></button>").addClass("btn btn-danger btn-xs  dele_dutton")
            .append($(" <span></span>").addClass("glyphicon glyphicon-remove-circle")).append("删除");
        var dbbutton = $("<td></td>").append(dbutton).append(" ").append(debutton);
        $("<tr></tr>")
            .append(dempName)
            .append(dgender)
            .append(demail)
            .append(dempid)
            .append(ddeptName)
            .append(dbbutton)
            .appendTo("#mytable tbody");

    });

}

function build_page_info(result) {

    $("#pageinfo_area").empty();
    $("#pageinfo_area").append("当前" + result.extend.pageinfo.pageNum + ",总" + result.extend.pageinfo.pages + "页,总" + result.extend.pageinfo.total + "条记录");
    pagetotal = result.extend.pageinfo.pages;
}

function build_pagenav_info(result) {
    $("#pageinfog").empty();
    var ur = $("<ul></ul>").addClass("pagination");
    var firstpage = $("<li></li>").append($("<a></a>").append("首页").attr("href", "#"));
    var prepage = $("<li></li>").append($("<a></a>").append("&raquo;"));
    if (result.extend.pageinfo.hasPreviousPage == false) {
        firstpage.addClass("disabled");
        prepage.addClass("disabled");
    }
    else {
        firstpage.click(function () {
            to_page(1);
        });
        prepage.click(function () {
            to_page(result.extend.pageinfo.pageNum + 1);
        });
    }
    var nextpage = $("<li></li>").append($("<a></a>").append("&laquo;"));
    var lastpage = $("<li></li>").append($("<a></a>").append("末页").attr("href", "#"));
    if (result.extend.pageinfo.hasNextPage == false) {
        lastpage.addClass("disabled");
        nextpage.addClass("disabled");
    }
    else {
        nextpage.click(function () {
            to_page(result.extend.pageinfo.pageNum - 1);
        });
        lastpage.click(function () {
            to_page(result.extend.pageinfo.pages);
        });
    }
    ur.append(firstpage).append(nextpage);
    var emp = result.extend.pageinfo.navigatepageNums;
    $.each(emp, function (index, items) {
        var f = $("<li></li>").append($("<a></a>").append(items));
        if (result.extend.pageinfo.pageNum == items) {
            f.addClass("active");
        }
        f.click(function () {
            to_page(items);
        });
        ur.append(f);
    });
    ur.append(prepage).append(lastpage);
    var nav = $("<nav></nav>").append(ur);
    nav.appendTo("#pageinfog");
}

/*  点击弹出模态框*/
$("#emp_add_model").click(function () {
    /*  将表单数据重置*/
    $("#empaddmodel form")[0].reset();
    /*  发送ajax请求查出部门信息*/
    getdeptname();
    $("#empaddmodel").modal({
        backdrop: "static"
    });
});
/* 保存按钮 */
/*  检验名字是否重复 */
$("#inputempname").change(function () {
    var empName = this.value;
    $.ajax({
        url: "${app_context}/my/check",
        data: "empName=" + empName,
        type: "POST",
        success: function (result) {
            if (result.code == 100) {
                show_validator_msg("#inputempname", "success", "用户名可用");
                $("#emp_save").attr("ajax_va", "success");
            }
            else if (result.code == 200) {
                show_validator_msg("#inputempname", "error", "用户名不可用");
                $("#emp_save").attr("ajax_va", "error");
            }
        }
    });
});

/*  数据校验*/
function validate_add_form() {
    var empname = $("#inputempname").val();
    var pempname = /(^[a-zA-Z0-9_-]{6,10}$)|(^[\u2E80-\u9FFF]{2,5})/;
    if (!pempname.test(empname)) {
        show_validator_msg("#inputempname", "error", "中文名2-5位，英文6-10");
        /*  alert("中文名2-5位，英文6-10"); */
        return false;
    }
    else {
        show_validator_msg("#inputempname", "success", "");
        /*   $("#inputempname").parent().addClass("has-success");
           $("#inputempname").next("span").text(""); */
    }
    var emailcheck = $("#inputemail").val();
    var emailregulator = /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/;
    if (!emailregulator.test(emailcheck)) {
        /*   alert("邮箱格式不正确"); */
        show_validator_msg("#inputemail", "error", "邮箱格式不正确");
        return false;
    }
    else {
        show_validator_msg("#inputemail", "success", "");
    }
    return true;
}

function show_validator_msg(ele, status, msg) {
    $(ele).parent().removeClass("has-success  has-error");
    $(ele).next("span").text("");
    if (status == "success") {
        $(ele).parent().addClass("has-success");
        $(ele).next("span").text("");
    }
    else if (status == "error") {
        $(ele).parent().addClass("has-error");
        $(ele).next("span").text(msg);
    }
}

$("#emp_save").click(function () {
    /*  提交数据前对数据进行校验*/
    if (!validate_add_form()) {
        return false;
    }
    /*        判断之前的ajax请求是否成功 */
    if ($(this).attr("ajax_va") == "error") {
        return false;
    }
    $.ajax({
        url: "${app_context}/my/save",
        type: "POST",
        data: $("#empaddmodel form").serialize(),
        success: function (result) {
            if (result.code) {
                $("#empaddmodel").modal('hide');
                to_page(pagetotal);
            }
            else {
                if (undefined == result.extend.errofail.email) {
                    show_validator_msg("#inputemail", "error", result.extend.errofail.email);
                }
                if (undefined == result.extend.errofail.empName) {
                    show_validator_msg("#inputempname", "error", result.extend.errofail.empName);
                }
            }
        }
    });
});

function getdeptname() {
    $.ajax({
        url: "${app_context}/my/deptname",
        type: "GET",
        success: function (result) {
            /* 	{"code":100,"msg":"成功","extend":{"depts":[{"deptId":1,"deptName":"研发部"},{"deptId":2,"deptName":"测试部"}]}} */
            $.each(result.extend.depts, function () {
                var option = $("<option></option>").append(this.deptName).attr("value", this.deptId);
                var option1 = $("<option></option>").append(this.deptId);
                option1.appendTo("#deptaddid");
                option.appendTo("#deptadd");
            });
        }
    });
}

$(document).on("click", ".edi_dutton", function () {
    $("#empaddmodel").modal({
        backdrop: "static"
    });
});