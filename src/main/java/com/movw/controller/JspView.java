package com.movw.controller;

import com.movw.beans.News;
import com.movw.beans.User;
import com.movw.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class JspView {
    @Autowired
    private AdminService adminService;

    /**
     * 跳到home
     *
     * @return
     */
    @RequestMapping("/home")
    public String toHome() {
        return "home";
    }

    /**
     * 跳转用户登录页面
     *
     * @return
     */
    @RequestMapping("/login")
    public String toUserLogin() {
        return "login";
    }

    /**
     * 跳转管理员登录页面
     *
     * @return
     */
    @RequestMapping("/adminlogin")
    public String toAdminLogin() {
        return "Adminlogin";
    }
    /**
     * 跳转管理员页面
     *
     * @return
     */
    @RequestMapping("/news_back")
    public String toAdminnews_back() {
        return "news_back";
    }

    /**
     * 新闻详情
     *
     * @param newsid
     * @param model
     * @return
     */
    @RequestMapping(value = "/newsitem", method = RequestMethod.GET)
    public String toNewsitem(Integer newsid, Model model) {
        News news = adminService.findNewsById(newsid);
        model.addAttribute(news);
        return "newsitem";
    }

    /**
     * 用户浏览新闻列表
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/newslist", method = RequestMethod.GET)
    public String toNewslist(Model model) {
        List<News> newslist = adminService.selectNews();
        System.out.println(newslist.size() + "hwoowowow");
        model.addAttribute(newslist);
        return "usernewslist";
    }

    /**
     * 个人信息中心
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/userinfo", method = RequestMethod.GET)
    public String toUserInfo(Model model, HttpSession session) {
        String username = null;
        try {
             username = (String) session.getAttribute("username");
            User user = adminService.findUserByName(username);
            model.addAttribute(user);
        } catch (Exception e) {
            return "login";
        }
        return "userInfo";
    }
}
