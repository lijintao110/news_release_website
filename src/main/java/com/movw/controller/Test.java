package com.movw.controller;

import com.movw.beans.Admin;
import com.movw.beans.User;
import com.movw.service.AdminService;
import com.movw.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
public class Test {
    @Autowired
    private UserService userService;
    @Autowired
    private AdminService adminService;
    @RequestMapping(value = "/test")
    @ResponseBody
    public List<User> testGetUserById() {
        List<Admin> list = new ArrayList<Admin>();
        List<User> list1 = new ArrayList<User>();
        list = userService.select();
        list1=adminService.selectUser();
        System.out.println("-----------------------");
//        System.out.println(list.size());
        System.out.println(list.size());
        System.out.println("-----------------------");
        return list1;
    }

    @RequestMapping("/itemEdit/{id}")
    @ResponseBody
// 如果id和方法的形参一致，@PathVariable注解中可以不写内容
    public Admin editItem(@PathVariable("id") Integer iid) {
        // 调用服务
        Admin items = userService.findById(iid);
        // 把数据传递给页面，需要用到Model接口
//        model.addAttribute("item", items);
        // 返回逻辑视图
        return items;
    }
    @RequestMapping("/itemEdit/model/{id}")
// 如果id和方法的形参一致，@PathVariable注解中可以不写内容
    public String  editItem(@PathVariable("id") Integer iid,Model model) {
        // 调用服务
        Admin items = userService.findById(iid);
        // 把数据传递给页面，需要用到Model接口
        model.addAttribute("item", items);
        // 返回逻辑视图
        return "holo";
    }

}
