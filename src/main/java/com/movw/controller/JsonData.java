package com.movw.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.movw.beans.News;
import com.movw.beans.News_category;
import com.movw.service.AdminService;
import com.movw.utils.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class JsonData {
    @Autowired
    private AdminService adminService;

    @RequestMapping("/newslist")
    @ResponseBody
    public List<News> getNewsTitle() {
        List<News> newslist = adminService.selectNews();
        return newslist;
    }

    @RequestMapping("/newslist1")
    @ResponseBody
    public Msg getempwithjson(@RequestParam(value="pn",defaultValue="1")Integer pn)
    {
        PageHelper.startPage(pn, 6);
        List<News> newslist = adminService.selectNews();
        // System.out.println("email"+emp.get(0).getEmail());
        PageInfo page = new PageInfo(newslist,6);
        return  Msg.success().add("pageinfo",page);
    }

    @RequestMapping("/news_category")
    @ResponseBody
    public List<News_category> getNews_category() {
        List<News_category> newslist = adminService.selectUser_category();
        return newslist;
    }
}
