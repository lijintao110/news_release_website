package com.movw.dao;

import com.movw.beans.Admin;

import java.util.List;

/**
 * @author shkstart
 * @create 2018-09-20 16:28
 */
public interface AdminDao {
    //添加管理员
     void addAdmin(Admin admin);
    //查找管理员
     Admin findByName(String adminName);
    //查找管理员
    Admin findById(Integer id);
    //删除管理信息
     void deleteByName(String adminName);
    //修改管理员信息
     void updateById(Integer adminId, String adminPassword);
    //显示所有管理员
     List<Admin> select();
}
