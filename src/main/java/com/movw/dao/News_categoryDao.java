package com.movw.dao;

import com.movw.beans.News_category;

import java.util.List;

/**
 * @author shkstart
 * @create 2018-09-20 16:28
 */
public interface News_categoryDao {
    //添加类别
    public void addNews_category(News_category news_category);

    //显示所有类别
    public List<News_category> select();

    //查找管理员
    public News_category findByName(String news_category_name);

}
