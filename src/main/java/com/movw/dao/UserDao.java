package com.movw.dao;

import com.movw.beans.User;

import java.util.List;

/**
 * @author shkstart
 * @create 2018-09-20 16:28
 */
public interface UserDao {
    //添加用户
    int addUser(User user);
    //查找用户
    User findUserByName(String name);
    //查找用户
    User findUserById(Integer id);
    //删除用户
    int deleteUserById(Integer id);
    //修改用户信息
    int updateUser(User user);
    //显示所有用户
    List<User> selectUser();
}
