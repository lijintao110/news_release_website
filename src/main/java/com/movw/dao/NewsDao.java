package com.movw.dao;

import com.movw.beans.News;

import java.util.List;

public interface NewsDao {
    //添加新闻
    int addNews(News news);
    //查找新闻
    News findNewsById(Integer id);
    //删除新闻
    int deleteNewsById(Integer id);
    //显示新闻
    List<News> selectNews();
}
