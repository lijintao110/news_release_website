package com.movw.dao;

import com.movw.beans.News_comment;

import java.util.List;

/**
 * @author shkstart
 * @create 2018-09-20 16:28
 */
public interface News_commentDao {
    //添加评论
    public void addNews_comment(News_comment news_comment);

    //查找评论
    public List<News_comment> findByNameNews_comment(Integer news_id);

    //删除评论信息
    public void deleteById(Integer newscomment_id);
}
