package com.movw.service;

import com.movw.beans.Admin;
import com.movw.dao.AdminDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service("userService")
public class UserServiceImp implements UserService {
    @Autowired
    private AdminDao userMapper;
    @Override
    public List<Admin> select() {

        return userMapper.select();
    }

    @Override
    public Admin findById(Integer id) {
        return userMapper.findById(id);
    }
}
