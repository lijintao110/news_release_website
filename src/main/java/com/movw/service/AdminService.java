package com.movw.service;

import com.movw.beans.*;

import java.util.List;

public interface AdminService {
    //添加管理员
    int  addAdmin(Admin admin);
    //查找管理员
    Admin findByName(String adminName);
    //查找管理员
    Admin findById(Integer id);
    //删除管理信息
    int deleteByName(String adminName);
    //修改管理员信息
    int updateById(Integer adminId, String adminPassword);
    //显示所有管理员
    List<Admin> selectAdmin();

    //添加用户
    int addUser(User user);
    //查找用户
    User findUserByName(String name);
    //查找用户
    User findUserById(Integer id);
    //删除用户
    int deleteUserById(Integer id);
    //修改用户信息
    int updateUser(User user);
    //显示所有用户
    List<User> selectUser();

    //添加新闻
    int addNews(News news);
    //查找新闻
    News findNewsById(Integer id);
    //删除新闻
    int deleteNewsById(Integer id);
    //显示新闻标题
    List<News> selectNews();

    //添加评论
    int addNewsComment(News_comment news_comment);
    //删除评论
    int deleteNewsComment(Integer id);
    //显示评论
    List<News_comment> selectCommentByNewsId(Integer news_id);
    //添加类别
    public void addNews_category(News_category news_category);

    //显示所有类别
    public List<News_category> selectUser_category();

}
