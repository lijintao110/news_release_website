package com.movw.service;

import com.movw.beans.*;
import com.movw.dao.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("adminService")
public class AdminServiceImp implements AdminService {
    @Autowired
    private AdminDao adminDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private NewsDao newsDao;
    @Autowired
    private News_commentDao news_commentDao;
    @Autowired
    private News_categoryDao news_categoryDao;
    @Autowired
    private User_lookDao user_lookDao;

    @Override
    public int addAdmin(Admin admin) {
        try {
            adminDao.addAdmin(admin);
        } catch (Exception e) {
            return 0;
        }
        return 1;
    }

    @Override
    public Admin findByName(String adminName) {
        return adminDao.findByName(adminName);
    }

    @Override
    public Admin findById(Integer id) {
        return adminDao.findById(id);
    }

    @Override
    public int deleteByName(String adminName) {
        try {
            adminDao.deleteByName(adminName);
        } catch (Exception e) {
            return 0;
        }
        return 1;
    }

    @Override
    public int updateById(Integer adminId, String adminPassword) {
        try {
            adminDao.updateById(adminId, adminPassword);
        } catch (Exception e) {
            return 0;
        }
        return 1;
    }

    @Override
    public List<Admin> selectAdmin() {
        return adminDao.select();
    }

    @Override
    public int addUser(User user) {
        try {
            userDao.addUser(user);
        } catch (Exception e) {
            return 0;
        }
        return 1;
    }

    @Override
    public User findUserByName(String name) {
        return userDao.findUserByName(name);
    }

    @Override
    public User findUserById(Integer id) {
        return userDao.findUserById(id);
    }

    @Override
    public int deleteUserById(Integer id) {
        try {
            userDao.deleteUserById(id);
        } catch (Exception e) {
            return 0;
        }
        return 1;
    }

    @Override
    public int updateUser(User user) {
        try {
            userDao.updateUser(user);
        } catch (Exception e) {
            return 0;
        }
        return 1;
    }

    @Override
    public List<User> selectUser() {
        return userDao.selectUser();
    }

    @Override
    public int addNews(News news) {
        try {
            newsDao.addNews(news);
        } catch (Exception e) {
            return 0;
        }
        return 1;
    }

    @Override
    public News findNewsById(Integer id) {
        return newsDao.findNewsById(id);
    }

    @Override
    public int deleteNewsById(Integer id) {
        try {
            newsDao.deleteNewsById(id);
        } catch (Exception e) {
            return 0;
        }
        return 1;
    }

    @Override
    public List<News> selectNews() {
        return newsDao.selectNews();
    }

    @Override
    public int addNewsComment(News_comment news_comment) {
        try {
            news_commentDao.addNews_comment(news_comment);
        } catch (Exception e) {
            return 0;
        }
        return 1;
    }

    @Override
    public int deleteNewsComment(Integer id) {
        try {
            news_commentDao.deleteById(id);
        } catch (Exception e) {
            return 0;
        }
        return 1;
    }

    @Override
    public List<News_comment> selectCommentByNewsId(Integer news_id) {
        return news_commentDao.findByNameNews_comment(news_id);
    }

    @Override
    public void addNews_category(News_category news_category) {
        news_categoryDao.addNews_category(news_category);
    }

    @Override
    public List<News_category> selectUser_category() {
        return news_categoryDao.select();
    }
}
