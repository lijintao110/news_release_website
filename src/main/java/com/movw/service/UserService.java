package com.movw.service;

import com.movw.beans.Admin;

import java.util.List;

public interface UserService {
    List<Admin> select();
    //查找管理员
    Admin findById(Integer id);
}
