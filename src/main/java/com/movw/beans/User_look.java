package com.movw.beans;

import java.util.Date;

/**
 * @author shkstart
 * @create 2018-09-20 11:29
 * 用户浏览信息
 */
public class User_look {
    private Integer user_id;
    private Integer news_id;
    private Date look_date;

    public User_look() {
    }

    public User_look(Integer user_id, Integer news_id, Date look_date) {
        this.user_id = user_id;
        this.news_id = news_id;
        this.look_date = look_date;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Integer getNews_id() {
        return news_id;
    }

    public void setNews_id(Integer news_id) {
        this.news_id = news_id;
    }

    public Date getLook_date() {
        return look_date;
    }

    public void setLook_date(Date look_date) {
        this.look_date = look_date;
    }
}
