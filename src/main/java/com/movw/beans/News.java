package com.movw.beans;

import java.util.Date;

public class News {
    private Integer newsid;
    private String newsitem;//新闻标题
    private String newsauthor;//作者
    private Date newsdate;//发布时间
    private String newscontent;//内容
    private Integer newsreadnum;//已读人数
    public News(){}
    public News(Integer newsid, String newsitem, String newsauthor, Date newsdate, String newscontent, Integer newsreadnum){
        this.newsid=newsid;
        this.newsitem=newsitem;
        this.newsauthor=newsauthor;
        this.newsdate=newsdate;
        this.newscontent=newscontent;
        this.newsreadnum=newsreadnum;
    }

    public String getNewsauthor() {
        return newsauthor;
    }

    public void setNewsauthor(String newsauthor) {
        this.newsauthor = newsauthor;
    }

    public Date getNewsdate() {
        return newsdate;
    }

    public String getNewscontent() {
        return newscontent;
    }

    public Integer getNewsid() {
        return newsid;
    }

    public String getNewsitem() {
        return newsitem;
    }

    public Integer getNewsreadnum() {
        return newsreadnum;
    }

    public void setNewsdate(Date newsdate) {
        this.newsdate = newsdate;
    }

    public void setNewscontent(String newscontent) {
        this.newscontent = newscontent;
    }

    public void setNewsid(Integer newsid) {
        this.newsid = newsid;
    }

    public void setNewsitem(String newsitem) {
        this.newsitem = newsitem;
    }

    public void setNewsreadnum(Integer newsreadnum) {
        this.newsreadnum = newsreadnum;
    }
}
