package com.movw.beans;

import java.util.Date;

/**
 * @author shkstart
 * @create 2018-09-20 12:14
 * 新闻评论
 */
public class News_comment {
    private Integer news_comment_id;
    private Integer news_id;//新闻的id
    private Integer user_id;//用户id
    private Date news_comment_date;//评论时间
    private String comment_content;//评论内容

    public News_comment() {
    }

    public News_comment(Integer news_id, Integer user_id, Date news_comment_date, String comment_content) {
        this.news_id = news_id;
        this.user_id = user_id;
        this.news_comment_date = news_comment_date;
        this.comment_content = comment_content;
    }

    public Integer getNews_comment_id() {
        return news_comment_id;
    }

    public void setNews_comment_id(Integer news_comment_id) {
        this.news_comment_id = news_comment_id;
    }

    public Integer getNews_id() {
        return news_id;
    }

    public void setNews_id(Integer news_id) {
        this.news_id = news_id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Date getNews_comment_date() {
        return news_comment_date;
    }

    public void setNews_comment_date(Date news_comment_date) {
        this.news_comment_date = news_comment_date;
    }

    public String getComment_content() {
        return comment_content;
    }

    public void setComment_content(String comment_content) {
        this.comment_content = comment_content;
    }
}
