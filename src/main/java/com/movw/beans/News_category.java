package com.movw.beans;

/**
 * @author shkstart
 * @create 2018-09-20 11:30
 * 新闻类别
 */
public class News_category {
    private Integer news_category_id;
    private String news_category_name;

    public News_category() {
    }

    public News_category(Integer news_category_id, String news_category_name) {
        this.news_category_id = news_category_id;
        this.news_category_name = news_category_name;
    }

    public Integer getNews_category_id() {
        return news_category_id;
    }

    public void setNews_category_id(Integer news_category_id) {
        this.news_category_id = news_category_id;
    }

    public String getNews_category_name() {
        return news_category_name;
    }

    public void setNews_category_name(String news_category_name) {
        this.news_category_name = news_category_name;
    }
}
