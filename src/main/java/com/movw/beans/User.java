package com.movw.beans;

public class User {
    private Integer userid;
    private String username;
    private String userpwd;
    private String usertel;
    private String usersex;

    public User() {
    }

    public User(Integer userid, String username, String userpwd, String usersex, String userlevel) {
        this.userid = userid;
        this.username = username;
        this.userpwd = userpwd;
        this.usertel = usertel;
        this.usersex = usersex;
    }


    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getUserid() {
        return userid;
    }

    public String getUsersex() {
        return usersex;
    }

    public String getUsertel() {
        return usertel;
    }

    public void setUsersex(String usersex) {
        this.usersex = usersex;
    }

    public void setUsertel(String usertel) {
        this.usertel = usertel;
    }

    public void setUserpwd(String userpwd) {
        this.userpwd = userpwd;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserpwd() {
        return userpwd;
    }

    public String getUsername() {
        return username;
    }
}

